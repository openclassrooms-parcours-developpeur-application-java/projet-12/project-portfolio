package com.projectportfolio.model;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "project")
public class ProjectEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private Timestamp publication;

    private String summarize;

}
