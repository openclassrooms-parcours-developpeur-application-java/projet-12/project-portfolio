package com.projectportfolio.repository;

import com.projectportfolio.model.ResourceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<ResourceEntity, Integer> {


    /**
     * OBTENIR TOUTES LES RESSOURCES PAR L'ID DU PROJET
     * @param projectId
     * @return
     */
    @Query(value = "SELECT DISTINCT resource.* FROM resource" +
            " INNER JOIN resource_project ON resource_project.project_id= :projectId" +
            " WHERE resource.id= resource_project.resource_id", nativeQuery = true)
    List<ResourceEntity> findAllResourceByProjectId(@Param("projectId") Integer projectId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE RESOURCE ET PROJECT EXISTE DEJA
     * @param resource_id
     * @param projectId
     * @return
     */
    @Query(value = "SELECT count(resource_project) > 0 FROM resource_project" +
            " WHERE resource_project.resource_id= :resource_id AND resource_project.project_id= :projectId", nativeQuery = true)
    Boolean resourceProjectExists(@Param("resource_id") Integer resource_id, @Param("projectId") Integer projectId);


    /**
     * LIER DANS LA TABLE DE JOINTURE LA RESSOURCE AU PROJET
     * @param resourceId
     * @param projectId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO resource_project (resource_id, project_id)" +
            " VALUES (:resourceId, :projectId)", nativeQuery = true)
    int joinResourceToTheProject(@Param("resourceId") Integer resourceId, @Param("projectId") Integer projectId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE RESSOURCE ET UN PROJET
     * @param resourceId
     * @param projectId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM resource_project" +
            " WHERE resource_id= :resourceId AND project_id= :projectId", nativeQuery = true)
    int deleteResourceByProject(@Param("resourceId") Integer resourceId, @Param("projectId") Integer projectId);


    /**
     * VERIFIE SI LE PROJECT EXISTE
     * @param projectId
     * @return
     */
    @Query(value = "SELECT COUNT(project) > 0 FROM project WHERE project.id= :projectId", nativeQuery = true)
    Boolean projectExists(@Param("projectId") Integer projectId);


    /**
     * OBTENIR LA RESSOURCE PAR SON URL
     * @param url
     * @return
     */
    ResourceEntity findByUrl(String url);

}
